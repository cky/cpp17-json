A basic JSON implementation using `std::variant`
===

This library implements JSON as specified in [RFC 7159]. It implements
only the required functionality and nothing extra. At the C++ side, the
object types are exposed as `json::value<T>`, `json::array<T>`, and
`json::object<T>`, where `T` is the underlying character type. UTF-8,
UTF-16, and UTF-32 are supported via the use of an 8-bit, 16-bit, or
32-bit integral type.

C++ variants are type-safe discriminated unions: the type of the value
is always known, and of a limited set of possible types (in the case of
JSON, those types are null, boolean, number, string, array, and object).
In particular, `json::value<T>` is a `std::variant<std::nullptr_t,
bool, double, std::basic_string<T>, json::array<T>, json::object<T>>`,
`json::array<T>` is a `std::vector<json::value<T>>`, and `json::object<T>`
is a `std::unordered_map<std::basic_string<T>, json::value<T>>`.

Boost has a variant implementation, [Boost.Variant], for a long time.
C++1z (C++17 or whatever it will end up being called) will also have a
variant facility, currently specified in [P0088R3], which is at first
glance similar to Boost.Variant, but differs in many details.

This library has been tested under Visual Studio 2017, Xcode 10, and
gcc 7. Your mileage may vary with other systems.

This library is written by [Chris Jester-Young], and everything here is
published under the [Boost Software Licence]. I've also included a copy
of [Catch] in-tree; it's also licensed under the same licence, but not
written by me (obviously).

[RFC 7159]: https://www.rfc-editor.org/rfc/rfc7159.txt
[Boost.Variant]: http://www.boost.org/doc/html/variant.html
[P0088R3]: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0088r3.html
[Boost Software Licence]: http://www.boost.org/LICENSE_1_0.txt
[Catch]: http://www.catch-lib.net/
[Chris Jester-Young]: http://about.cky.nz/
